# README #

Kurento module to mix local audio files.

### Full Requirements ###

* Build a new gstreamer kurento module
* It has a single audio src pad: no video, no sinks
* Can be prepended to WebRtcEndpoint (and HttpEndpoint)
* Has a method that takes a String, and based on that choose what to play (in particular will take a json with an array of {"track":"path", "volume":vol} and will mix the N tracks)
* Has three methods play, pause, stop
* It will trigger EndOfStream
* Has to be instantiated from the js client
* (Has a method seek(position))

### Installation ###

From project root folder:

* debuild -us -uc
* sudo dpkg -i ../amix_0.0.1~rc1_amd64.deb