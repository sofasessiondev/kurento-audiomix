#ifndef _GST_AMIX_H_
#define _GST_AMIX_H_

#include <commons/kmselement.h>

G_BEGIN_DECLS

#define GST_TYPE_AMIX   (gst_amix_get_type())
#define KMS_TYPE_AMIX_ENDPOINT_STATE   (gst_amix_get_type())
#define GST_AMIX(obj)   (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_AMIX,Gstamix))
#define GST_AMIX_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_AMIX,GstamixClass))
#define GST_IS_AMIX(obj)   (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_AMIX))
#define GST_IS_AMIX_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_AMIX))
typedef struct _Gstamix Gstamix;
typedef struct _GstamixClass GstamixClass;
typedef struct _GstamixPrivate GstamixPrivate;

struct _Gstamix
{
  KmsElement parent;

  /*< private > */
  GstamixPrivate *priv;
};

struct _GstamixClass
{
  KmsElementClass parent_class;
};

GType gst_amix_get_type (void);

gboolean gst_amix_plugin_init (GstPlugin * plugin);


G_END_DECLS
#endif /* _GST_AMIX_H_ */
