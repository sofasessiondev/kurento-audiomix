#include "gstamix.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/base/gstbasesrc.h>
#include <glib/gstdio.h>
#include <memory>
#include <string.h>
#include <commons/kmsagnosticcaps.h>
#include <jansson.h>

GST_DEBUG_CATEGORY_STATIC (gst_amix_debug_category);
#define GST_CAT_DEFAULT gst_amix_debug_category
#define PLUGIN_NAME "amix"
#define APPSRC_DATA "appsrc_data"
#define APPSINK_DATA "appsink_data"
#define BASE_TIME_DATA "base_time_data"
#define MAX_SRCS_FILES 20

#define GST_AMIX_GET_PRIVATE(obj) (    \
    G_TYPE_INSTANCE_GET_PRIVATE (      \
        (obj),                         \
        GST_TYPE_AMIX,                 \
        GstamixPrivate))

typedef enum {
  KMS_AMIX_END_POINT_STATE_STOP,
  KMS_AMIX_END_POINT_STATE_START,
  KMS_AMIX_END_POINT_STATE_PAUSE
} KmsAmixEndPointState;

struct _GstamixPrivate {
	KmsAmixEndPointState state;	//current state
	GstElement *pipeline;
	GMutex base_time_lock;
	GstElement *audiomixer;		//gstreamer component to mix audios
	GstPad *sinksMixer[MAX_SRCS_FILES];	// sink pads of the audiomixer
	double volumes[MAX_SRCS_FILES];	//volumes of each sink pad of audiomixers
	gchar *tracks;	//string json tracks
};

G_DEFINE_TYPE_WITH_CODE (Gstamix, gst_amix,
		KMS_TYPE_ELEMENT,
		GST_DEBUG_CATEGORY_INIT (gst_amix_debug_category,
				PLUGIN_NAME, 0,
				"debug category for amix element") );

enum {
	PROP_0,
	PROP_TRACKS, //JSON of the tracks
    PROP_STATE,
	N_PROPERTIES
};
static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

#define GST_TYPE_AMIX_ENDPOINT_STATE (gst_amix_state_get_type ())	//this for other kms components is created automatically, but didn't get how, for example: KMS_TYPE_URI_ENDPOINT_STATE
static GType gst_amix_state_get_type(void) {
	static GType state_type = 0;
	if (!state_type) {
		static GEnumValue state_types[] = {
				{ KMS_AMIX_END_POINT_STATE_STOP, "stop", "stop" },
				{ KMS_AMIX_END_POINT_STATE_START, "start", "start" },
				{ KMS_AMIX_END_POINT_STATE_PAUSE, "pause", "pause" },
				{ 0, NULL, NULL }, };

		state_type = g_enum_register_static("KmsAmixEndPointState", state_types);
	}
	return state_type;
}
#define DEFAULT_AMIX_ENDPOINT_STATE KMS_AMIX_END_POINT_STATE_STOP

#define BASE_TIME_LOCK(obj) (                                           \
  g_mutex_lock (&GST_AMIX(obj)->priv->base_time_lock)        \
)

#define BASE_TIME_UNLOCK(obj) (                                         \
  g_mutex_unlock (&GST_AMIX(obj)->priv->base_time_lock)      \
)

static GstPadProbeReturn negotiate_appsrc_caps(GstPad * pad,
		GstPadProbeInfo * info, gpointer element) {
	GstQuery *query = GST_PAD_PROBE_INFO_QUERY(info);
	GstElement *appsrc = GST_ELEMENT(element);
	GstPad *srcpad;

	switch (GST_QUERY_TYPE(query)) {
	case GST_QUERY_CAPS:
	case GST_QUERY_ACCEPT_CAPS:
		break;
	default:
		return GST_PAD_PROBE_OK;
	}

	query = gst_query_make_writable(query);
	srcpad = gst_element_get_static_pad(appsrc, "src");
	/* Send query to the agnosticbin */
	gst_pad_peer_query(srcpad, query);
	g_object_unref(srcpad);
	GST_PAD_PROBE_INFO_DATA(info) = query;

	return GST_PAD_PROBE_OK;
}

static void release_gst_clock(gpointer data) {
	g_slice_free(GstClockTime, data);
}

static GstPadProbeReturn set_appsrc_caps(GstPad * pad, GstPadProbeInfo * info,
		gpointer element) {
	GstEvent *event = GST_PAD_PROBE_INFO_EVENT(info);
	GstElement *appsrc = GST_ELEMENT(element);
	GstCaps *caps;

	if (GST_EVENT_TYPE(event) != GST_EVENT_CAPS) {
		return GST_PAD_PROBE_OK;
	}

	gst_event_parse_caps(event, &caps);
	if (caps == NULL) {
		GST_ERROR_OBJECT(pad, "Invalid caps received");
		return GST_PAD_PROBE_OK;
	}

	GST_DEBUG_OBJECT (appsrc, "Setting caps %" GST_PTR_FORMAT, caps);

	g_object_set(G_OBJECT(appsrc), "caps", caps, NULL);

	return GST_PAD_PROBE_OK;
}

static void gst_amix_init(Gstamix *self) {
	self->priv = GST_AMIX_GET_PRIVATE(self);
}

static GstFlowReturn new_sample_cb(GstElement * appsink, gpointer user_data) {
	GstElement *appsrc = GST_ELEMENT(user_data);
	GstFlowReturn ret;
	GstSample *sample;
	GstSegment *segment;
	GstBuffer *buffer;
	GstClockTime *base_time;
	GstPad *src, *sink;

	g_signal_emit_by_name(appsink, "pull-sample", &sample);

	if (sample == NULL) {
		GST_ERROR_OBJECT(appsink, "Cannot get sample");
		return GST_FLOW_OK;
	}

	buffer = gst_sample_get_buffer(sample);
	segment = gst_sample_get_segment(sample);

	if (buffer == NULL) {
		ret = GST_FLOW_OK;
		goto end;
	}

	gst_buffer_ref(buffer);

	buffer = gst_buffer_make_writable(buffer);

	if (GST_BUFFER_PTS_IS_VALID(buffer))
		buffer->pts = gst_segment_to_running_time(segment, GST_FORMAT_TIME,
				buffer->pts);
	if (GST_BUFFER_DTS_IS_VALID(buffer))
		buffer->dts = gst_segment_to_running_time(segment, GST_FORMAT_TIME,
				buffer->dts);

	BASE_TIME_LOCK(GST_OBJECT_PARENT(appsrc));

	base_time = (GstClockTime*) g_object_get_data(
			G_OBJECT(GST_OBJECT_PARENT(appsrc)),
			BASE_TIME_DATA);

	if (base_time == NULL) {
		GstClock *clock;

		clock = gst_element_get_clock(appsrc);
		base_time = g_slice_new0(GstClockTime);

		g_object_set_data_full(G_OBJECT(GST_OBJECT_PARENT(appsrc)),
		BASE_TIME_DATA, base_time, release_gst_clock);
		*base_time = gst_clock_get_time(clock)
				- gst_element_get_base_time(appsrc);

		g_object_unref(clock);
		GST_DEBUG ("Setting base time to: %" G_GUINT64_FORMAT, *base_time);
	}

	if (GST_BUFFER_DTS_IS_VALID(buffer)) {
		buffer->dts += *base_time;
	}

	if (GST_BUFFER_PTS_IS_VALID(buffer)) {
		buffer->pts += *base_time;
	} else {
		buffer->pts = buffer->dts;
	}

	BASE_TIME_UNLOCK(GST_OBJECT_PARENT(appsrc));

	src = gst_element_get_static_pad(appsrc, "src");
	sink = gst_pad_get_peer(src);

	if (sink != NULL) {
		if (GST_OBJECT_FLAG_IS_SET(sink, GST_PAD_FLAG_EOS)) {
			GST_INFO_OBJECT(sink, "Sending flush events");
			gst_pad_send_event(sink, gst_event_new_flush_start());
			gst_pad_send_event(sink, gst_event_new_flush_stop(FALSE));
		}
		g_object_unref(sink);
	}

	g_object_unref(src);

	g_signal_emit_by_name(appsrc, "push-buffer", buffer, &ret);

	gst_buffer_unref(buffer);

	if (ret != GST_FLOW_OK) {
		/* something wrong */
		GST_ERROR("Could not send buffer to appsrc %s. Cause: %s",
				GST_ELEMENT_NAME(appsrc), gst_flow_get_name(ret));
	}

	end: if (sample != NULL)
		gst_sample_unref(sample);

	return ret;
}

static void eos_cb(GstElement * appsink, gpointer user_data) {
	GstElement *appsrc = GST_ELEMENT(user_data);
	GstFlowReturn ret;
	GstPad *pad;

	GST_DEBUG_OBJECT(appsrc, "Sending eos event to main pipeline");

	g_signal_emit_by_name(appsrc, "end-of-stream", &ret);

	pad = gst_element_get_static_pad(appsrc, "src");

	if (pad != NULL) {
		gst_pad_send_event(pad, gst_event_new_flush_start());
		gst_pad_send_event(pad, gst_event_new_flush_stop(0));
		g_object_unref(pad);
	}

	GST_DEBUG_OBJECT(appsrc, "Returned %s", gst_flow_get_name(ret));
}

static GstPadProbeReturn internal_pipeline_probe(GstPad * pad,
	GstPadProbeInfo * info, gpointer element) {
	if (GST_PAD_PROBE_INFO_TYPE(info) & GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) {
		return set_appsrc_caps(pad, info, element);
	} else if (GST_PAD_PROBE_INFO_TYPE(info)
			& GST_PAD_PROBE_TYPE_QUERY_DOWNSTREAM) {
		return negotiate_appsrc_caps(pad, info, element);
	} else {
		GST_WARNING_OBJECT(pad, "Probe does nothing");
		return GST_PAD_PROBE_OK;
	}
}

static GstPadProbeReturn main_pipeline_probe(GstPad * pad,
	GstPadProbeInfo * info, gpointer element) {
	GstQuery *query = GST_PAD_PROBE_INFO_QUERY(info);
	GstElement *appsink = GST_ELEMENT(element);

	switch (GST_QUERY_TYPE(query)) {
	case GST_QUERY_CAPS:
	case GST_QUERY_ACCEPT_CAPS:
		break;
	default:
		return GST_PAD_PROBE_OK;
	}

	query = gst_query_make_writable(query);
	gst_element_query(appsink, query);
	GST_PAD_PROBE_INFO_DATA(info) = query;

	return GST_PAD_PROBE_OK;
}

static GstElement *
add_appsrc(Gstamix * self, GstElement * agnosticbin, GstElement * appsink) {
	GstElement *appsrc = NULL;
	GstPad *srcpad;

	/* Create appsrc element and link to agnosticbin */
	appsrc = gst_element_factory_make("appsrc", NULL);
	g_object_set(G_OBJECT(appsrc), "is-live", TRUE, "do-timestamp", TRUE,
			"min-latency", G_GUINT64_CONSTANT(0), "max-latency",
			G_GUINT64_CONSTANT(0), "format", GST_FORMAT_TIME, NULL);

	srcpad = gst_element_get_static_pad(appsrc, "src");
	gst_pad_add_probe(srcpad, GST_PAD_PROBE_TYPE_QUERY_UPSTREAM,
			main_pipeline_probe, appsink, NULL);
	g_object_unref(srcpad);

	gst_bin_add(GST_BIN(self), appsrc);
	if (!gst_element_link(appsrc, agnosticbin)) {
		GST_ERROR("Could not link %s to element %s", GST_ELEMENT_NAME(appsrc),
				GST_ELEMENT_NAME(agnosticbin));
	}

	gst_element_sync_state_with_parent(appsrc);

	return appsrc;
}

static GstElement *
get_agnostic_for_pad(Gstamix * self, GstPad * pad) {
	GstCaps *caps, *audio_caps = NULL;
	GstElement *agnosticbin = NULL;

	caps = gst_pad_query_caps(pad, NULL);

	if (caps == NULL) {
		return NULL;
	}

	audio_caps = gst_caps_from_string(KMS_AGNOSTIC_AUDIO_CAPS);

	if (gst_caps_can_intersect(audio_caps, caps)) {
		agnosticbin = kms_element_get_audio_agnosticbin(KMS_ELEMENT(self));
	}

	gst_caps_unref(caps);
	gst_caps_unref(audio_caps);

	return agnosticbin;
}

// triggered when decode bin creates a src pad
static void cb_new_pad(GstElement *element, GstPad *pad, Gstamix *self) {
	fprintf(stderr, "amix cb_new_pad\n");
	gchar *name = gst_element_get_name(element);

	//getting the index number from the name "decodebin_N"
	char *result = strstr(name, "_");
	int position = result - name;
	char sinkNum[3];
	strcpy(sinkNum, &name[position + 1]);
	sinkNum[2] = '\0';
	int index = atoi(sinkNum);
	fprintf(stderr, "new decodebin src pad! index: %d \n", index);

	gst_pad_link(pad, GST_PAD(self->priv->sinksMixer[index]));

	g_object_set (self->priv->sinksMixer[index], "volume", self->priv->volumes[index], NULL);
	g_free(name);
}

static void gst_amix_load(Gstamix * self){
	json_error_t error;
	json_t * root;

	root = json_loads(self->priv->tracks, 0, &error);
	json_t *jsonTracks = json_object_get(root, "tracks");

	if(!root)
	{
		fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
		return;
	}

	if (self->priv->audiomixer == NULL && self->priv->tracks != NULL) {
		fprintf(stderr, "amix mixer never created, creating with json\n %s..\n", self->priv->tracks);
		GstPadTemplate *audiomixer_sink_pad_template;

		self->priv->pipeline = gst_pipeline_new ("pipeline");
		self->priv->audiomixer = gst_element_factory_make("adder", "audiomixer");
		gst_bin_add(GST_BIN(self->priv->pipeline), self->priv->audiomixer);

		audiomixer_sink_pad_template = gst_element_class_get_pad_template(
				GST_ELEMENT_GET_CLASS(self->priv->audiomixer), "sink_%u");

		/* creating all srcs */
		unsigned int i;
		for (i = 0; i < json_array_size(jsonTracks); i++) {

			const char *fileSrc;
			json_t *jsonTrack = json_array_get(jsonTracks, i);
			fileSrc = json_string_value(json_object_get(jsonTrack, "path"));
			double volume = json_number_value(json_object_get(jsonTrack, "track_gain")) / 100;
			self->priv->volumes[i] = volume;
			fprintf(stderr, "amix creating src with %s %f\n", fileSrc, volume);

			char buf[8];
			sprintf(buf, "dbin_%d", i);

			GstElement *src = gst_element_factory_make("filesrc", NULL);
			GstElement *decodebin = gst_element_factory_make("decodebin", buf);

			g_object_set(src, "location", fileSrc,NULL);
			gst_bin_add(GST_BIN(self->priv->pipeline), src);
			gst_bin_add(GST_BIN(self->priv->pipeline), decodebin);
			gst_element_link_pads(src, "src", decodebin, "sink");
			self->priv->sinksMixer[i] = gst_element_request_pad(
					self->priv->audiomixer, audiomixer_sink_pad_template,
					"sink_%u", NULL);
			g_signal_connect(decodebin, "pad-added", G_CALLBACK(cb_new_pad), self);
		}

		/* settings gstreamer infrastructure */
		GstElement *appsink, *appsrc;
		GstElement *agnosticbin;
		GstPad *sinkpad;
		GstPad *pad = gst_element_get_static_pad(self->priv->audiomixer, "src");
		agnosticbin = get_agnostic_for_pad(self, pad);

		if (agnosticbin != NULL) {
			/* Create appsink */
			appsink = gst_element_factory_make("appsink", NULL);
			appsrc = add_appsrc(self, agnosticbin, appsink);

			g_object_set(appsink, "enable-last-sample", FALSE, "emit-signals",
					TRUE, "qos", FALSE, "max-buffers", 1, NULL);

			/* Connect new-sample signal to callback */
			g_signal_connect(appsink, "new-sample", G_CALLBACK(new_sample_cb),
					appsrc);
			g_signal_connect(appsink, "eos", G_CALLBACK(eos_cb), appsrc);

			g_object_set_data(G_OBJECT(pad), APPSINK_DATA, appsink);
			g_object_set_data(G_OBJECT(pad), APPSRC_DATA, appsrc);
		} else {
			GST_WARNING_OBJECT (self, "No supported pad: %" GST_PTR_FORMAT
					". Connecting it to a fakesink", pad);
			appsink = gst_element_factory_make("fakesink", NULL);
		}

		g_object_set(appsink, "sync", TRUE, "async", FALSE, NULL);

		sinkpad = gst_element_get_static_pad(appsink, "sink");

		if (agnosticbin != NULL) {
			gst_pad_add_probe(sinkpad,
					(GstPadProbeType)(
							GST_PAD_PROBE_TYPE_QUERY_DOWNSTREAM
									| GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM),
					internal_pipeline_probe, appsrc, NULL);
		}

		gst_bin_add(GST_BIN(self->priv->pipeline), appsink);
		gst_pad_link(pad, sinkpad);

		/* cleaning up */
		json_decref(jsonTracks);
		json_decref(root);
		g_object_unref(sinkpad);
		//g_object_unref(audiomixer_sink_pad_template);
		//TODO g_object_unref

		//gst_element_sync_state_with_parent(self->priv->audiomixer);
		gst_element_sync_state_with_parent(appsink);
	} else {
		fprintf(stderr, "amix mixer already created! Trying to change just volumes\n");

		unsigned int i;
		for (i = 0; i < json_array_size(jsonTracks); i++) {
			json_t *jsonTrack = json_array_get(jsonTracks, i);
			double volume = json_number_value(json_object_get(jsonTrack, "track_gain")) / 100;
			g_object_set (self->priv->sinksMixer[i], "volume", volume, NULL);
		}
	}
}

static void
gst_amix_change_state (Gstamix * self, KmsAmixEndPointState next)
{
  fprintf(stderr, "gst_amix_change_state " );
  fprintf(stderr, "current state %i - ", self->priv->state);
  fprintf(stderr, "next state %i\n", next);
  if (self->priv->state == next)
	  return;
  self->priv->state = next;
  switch (next) {
    case KMS_AMIX_END_POINT_STATE_STOP:
    	gst_element_set_state(self->priv->pipeline, GST_STATE_NULL);
    	BASE_TIME_LOCK (self);
    	g_object_set_data (G_OBJECT (self), BASE_TIME_DATA, NULL);
    	BASE_TIME_UNLOCK (self);
     	break;
    case KMS_AMIX_END_POINT_STATE_START:
    	gst_element_set_state(self->priv->pipeline, GST_STATE_PLAYING);
    	break;
    case KMS_AMIX_END_POINT_STATE_PAUSE:
    	gst_element_set_state(self->priv->pipeline, GST_STATE_PAUSED);
    	break;
  }
}

static void gst_amix_src_set_property(GObject * object, guint property_id,
	const GValue * value, GParamSpec * pspec) {

	Gstamix *self = GST_AMIX(object);

	KMS_ELEMENT_LOCK(KMS_ELEMENT(self));
	switch (property_id) {
	case PROP_TRACKS:
	      if (self->priv->tracks != NULL)
	    	  g_free (self->priv->tracks);
	      self->priv->tracks = g_value_dup_string (value);
	      gst_amix_load(self);
	      break;
	case PROP_STATE:
	   	   gst_amix_change_state (self, (KmsAmixEndPointState)g_value_get_enum (value));
	      break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
		break;
	}
	KMS_ELEMENT_UNLOCK(KMS_ELEMENT(self));
}

static void gst_amix_src_get_property(GObject * object, guint property_id,
	GValue * value, GParamSpec * pspec) {
	Gstamix *self = GST_AMIX(object);

	KMS_ELEMENT_LOCK(KMS_ELEMENT(self));
	switch (property_id) {
	case PROP_TRACKS:
	  g_value_set_string (value, self->priv->tracks);
	  break;
	case PROP_STATE:
	  g_value_set_enum (value, self->priv->state);
	  break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
		break;
	}
	KMS_ELEMENT_UNLOCK(KMS_ELEMENT(self));
}

/*
static void gst_amix_dispose(GObject * object) {
	Gstamix *self = GST_AMIX(object);

	//g_clear_object (&self->priv->loop);

	GstBus *bus;

	bus = gst_pipeline_get_bus(GST_PIPELINE(self));
	gst_bus_set_sync_handler(bus, NULL, NULL, NULL);
	g_object_unref(bus);

	gst_element_set_state(GST_ELEMENT(self), GST_STATE_NULL);

	g_mutex_clear(&self->priv->base_time_lock);

	//clean up as possible. May be called multiple times

	G_OBJECT_CLASS(gst_amix_parent_class)->dispose(object);
}
*/

static void gst_amix_class_init(GstamixClass *klass) {
	GstElementClass *gstelement_class;
	GObjectClass *gobject_class;
	gobject_class = G_OBJECT_CLASS(klass);
	//gobject_class->dispose = gst_amix_dispose;
	gobject_class->set_property = gst_amix_src_set_property;
	gobject_class->get_property = gst_amix_src_get_property;
	gstelement_class = GST_ELEMENT_CLASS(klass);
	gst_element_class_set_details_simple(
			gstelement_class, "A mix",
			"A mix", "A mix doc", "Alberto Pierini");

	obj_properties[PROP_STATE] = g_param_spec_enum ("state",
			  "Amix end point state",
			  "state of the amix end point element",
			  gst_amix_state_get_type(),//KMS_TYPE_AMIX_ENDPOINT_STATE, //commented KMS_TYPE_AMIX_ENDPOINT_STATE to avoid unused function error
			  DEFAULT_AMIX_ENDPOINT_STATE, G_PARAM_READWRITE);

	obj_properties[PROP_TRACKS] = g_param_spec_string("tracks",
				"Set json of the tracks",
				"Set json of the tracks",
				NULL,
				(GParamFlags)(G_PARAM_CONSTRUCT | G_PARAM_READWRITE));

	g_object_class_install_properties(gobject_class, N_PROPERTIES, obj_properties);

	g_type_class_add_private(klass, sizeof(GstamixPrivate));
}

gboolean gst_amix_plugin_init(GstPlugin *plugin) {
	GST_DEBUG_OBJECT(plugin, "gst_amix_plugin_init");
	return gst_element_register(plugin, PLUGIN_NAME, GST_RANK_NONE,GST_TYPE_AMIX);
}
