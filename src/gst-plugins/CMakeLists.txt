include_directories(
  ${KmsGstCommons_INCLUDE_DIRS}
  ${GSTREAMER_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}
  /home/bebo/src/kms-core/src/gst-plugins/
)

set(AMIX_SOURCES
  amix.c
  gstamix.cpp
  gstamix.h
)

add_library(amix MODULE ${AMIX_SOURCES})

target_link_libraries(amix
  ${KmsGstCommons_LIBRARIES}
  ${GSTREAMER_LIBRARIES}
  jansson
)

install(
  TARGETS amix
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_GST_PLUGINS_DIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)
